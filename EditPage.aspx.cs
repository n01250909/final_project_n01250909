﻿using System;
using System.Web;
using System.Web.UI;
using System.Data;

namespace Final_Project_MariiaKorolenko
{

    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //Set the values for the student
            DataRowView pagerow = GetPageInfo(pageid);
            if (pagerow == null)
            {
                title.InnerHtml = "No Pages Found.";
                return;
            }
            page_title.Text = pagerow["pagetitle"].ToString();
            page_content.Text = pagerow["pagecontent"].ToString();
           
           

        }
        
        protected void Edit_Page(object sender, EventArgs e)
        {
            string title = page_title.Text;
            string content = page_content.Text;

            string editquery = "Update pages set pagetitle='" + title + "'," +
                "pagecontent='" + content+"' where pageid="+pageid;
            debug.InnerHtml = editquery;
            
            page_edit.UpdateCommand = editquery;
            page_edit.Update();
            
        }
        
        protected DataRowView GetPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; 
            return pagerowview;

        }
        
    }
}
