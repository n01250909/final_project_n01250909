﻿using System;
using System.Web;
using System.Web.UI;
using System.Data;

namespace Final_Project_MariiaKorolenko
{

    public partial class PagesControl : System.Web.UI.UserControl
    {
        public string basequery = "select pageid, pagetitle from pages";
    
    
    
    
        protected void Page_Load(object Sender, EventArgs e)
        {
        
            pages_list.SelectCommand = basequery;
            DataView pageview = (DataView)pages_list.Select(DataSourceSelectArguments.Empty);
            
            string menuguts ="<li><a href=\"ListPages.aspx\">Home</a></li>";
            
            foreach(DataRowView row in pageview)
            {
                string id = row["pageid"].ToString();
                string title = row["pagetitle"].ToString();
                
                //<li><a href="Page.aspx">title</a></li>
                
                
          
                menuguts+="<li><a href=\"ViewPage.aspx?pageid="+id+"\">"+title+"</a></li>";
            }
            
            
            menucontainer.InnerHtml = menuguts;
        }
    }
}
