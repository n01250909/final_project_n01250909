﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_MariiaKorolenko
{

    public partial class ListPages : System.Web.UI.Page
    {

        public string query = "select pageid, pagetitle from pages";
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = query;
            pages_list.DataSource = Pages_Manual_Bind(pages_select);
            pages_list.DataBind();
        
        }
        
        
        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            //Takes a query, runs it into the database and returns an object that we can manipulate
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //test to see if we can manipulate something
                string id = row["pageid"].ToString();
                string title = row["pagetitle"].ToString();
                row["pagetitle"] = "<a href=\"ViewPage.aspx?pageid="+id+"\">"+title+"</a>";
            }
            
            myview = mytbl.DefaultView;

            return myview;
        }
        
        protected void New_Page(object sender, EventArgs e) 
        {
            Response.Redirect("AddPage.aspx",false);

        }
        
    }
}
