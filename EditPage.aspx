﻿<%@ Page Language="C#" Inherits="Final_Project_MariiaKorolenko.EditPage" AutoEventWireup="true" CodeFile="EditPage.aspx.cs" MasterPageFile="~/Site.master" %>
<asp:Content runat="server" ContentPlaceHolderID="container">

        <h1 id="title" runat="server"></h1><br>
            <asp:SqlDataSource runat="server" ID="page_select" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
            <asp:SqlDataSource runat="server" ID="page_edit" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
 
            <asp:TextBox id="page_title" runat="server" Placeholder="Page Title"/><br><br>
            <asp:TextBox id="page_content" TextMode="multiline" Columns="50" Rows="10" runat="server" Placeholder="Page Content"/><br><br>
            <asp:Button runat="server" Text="Edit Page" OnClick="Edit_Page"/><br><br>  
            
            <div id="debug" runat="server"></div>

</asp:Content>