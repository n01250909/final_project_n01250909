﻿<%@ Page Language="C#" Inherits="Final_Project_MariiaKorolenko.AddPage" AutoEventWireup="true" CodeFile="AddPage.aspx.cs" MasterPageFile="~/Site.master" %>

<asp:Content runat="server" ContentPlaceHolderID="container">

<asp:SqlDataSource runat="server" ID="insert_page" ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
        
        </asp:SqlDataSource>
               
        <h1>Add New Page</h1><br><br>
            <asp:TextBox id="page_title" runat="server" Placeholder="Page Title"/><br><br>
            <asp:TextBox id="page_content" TextMode="multiline" Columns="50" Rows="10" runat="server" Placeholder="Page Content"/><br><br>
            <asp:Button id="addpage" runat="server" Text="Add Page" OnClick="Add_Page"/><br><br>  
            
            <div id="debug" runat="server"></div>
          
</asp:Content>

